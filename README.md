# store_monitoring

| Service name | store_monitoring |
|------|-----|
| Language/Framework| Python/FastApi |
| Database | sqlite/PostgreSQL |

## Problem Statement: 
to create a **_dockerized_** service, **claim_process**  to process claims. 

## Requirements
1. **claim_process** transforms a JSON payload representing a single claim input with multiple lines and stores it into a RDB.
   - An example input (in CSV format) - *claim_1234.csv* is provided. Note that the names are not consistent in capitalization.
2. **claim_process** generates a unique id per claim.
3. **claim_process** computes the *“net fee”* as a result per the formula below.
*“net fee” = “provider fees” + “member coinsurance” + “member copay” - “Allowed fees”* (note again that the names are not consistent in capitalization).
4. A downstream service, **payments**, will consume *“net fee”* computed by **claim_process**.

## Local Setup
1. Clone the repository
   ```git clone https://gitlab.com/ae16b112/claim_process.git```
   
2. Create Virtual Environment in the project root
   ```python3 -m venv .venv```
3. Activate the Virtual Environment
   ```source .venv/bin/activate```

4. Create .env from sample.env

5. Install all dependencies
   ```pip3 install -r requirements.txt```

6. Run Server
    ```uvicorn src.main:app --reload```

7. Visit the docs panel http://127.0.0.1:8000/docs/
   The application will be available at http://localhost:8000.

## EndPoints
# POST /claims
This endpoint creates a new claim.

- Request Body
   - {
         "service_date": "string",
         "submitted_procedure": "string",
         "quadrant": "string",
         "plan_group_number": "string",
         "subscriber_number": "string",
         "provider_npi": "5555555555",
         "provider_fees": 10,
         "allowed_fees": 10,
         "member_coinsurance": 20,
         "member_copay": 20
      }

- Response
   - net_fee (float): The net fee for the claim

# GET /claims/{id}
This endpoint retrieves a specific claim by its unique ID.

- Parameters
   - id (int): The ID of the claim to retrieve.

- Response
   - claim

# GET /claims
This endpoint retrieves a list of all claims.

- Parameters

   - skip (int): The number of items to skip.
   - limit (int): The maximum number of items to retrieve.
- Response
   - [claims]



## how **claim_process** will communicate with **payments**.
# Failure handling
If there is a failure in either service, steps will be taken to unwind the process. For example, if there is a failure in the Claim Process service or payment service, the partially processed claim will be rolled back and the database will be left in the previous state. 

To handle failures in the communication between the services, the Claim Process service can retry the request a certain number of times before giving up. If it still fails, it can log the error and notify an administrator or take other corrective actions.

# Handling Multiple instances
If there are multiple instances of either service running concurrently to handle a large volume of claims, care must be taken to ensure that each instance is processing unique claims and that there are no conflicts in the database.

To handle this, the Claim Process service can use a distributed lock to ensure that only one instance is processing a particular claim at a time. This can be implemented using a library like redis-py or python-etcd.

Similarly, the Payments service can use a distributed lock to ensure that only one instance is processing a particular payment at a time.
















