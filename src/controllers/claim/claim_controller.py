from fastapi import HTTPException, Depends
from typing import List
from sqlalchemy.exc import SQLAlchemyError

from sqlmodel import Session, select
from ...database import get_session
from ...models.claims import Claim
from ...schemas.claims import Payment, ClaimInput
from ...services.index import ClaimService
from contextlib import closing


class ClaimController:
    def __init__(self):
        self.claim_service = ClaimService()
        
    def create_claim(self,claim: ClaimInput, session: Session = Depends(get_session)):
        try:
            net_fee = self.claim_service.calculate_net_fee(claim)
            db_claim = Claim.from_orm(claim)
            res = self.claim_service.create_claim(db_claim, session)

            # Call the payments API to process the payment
            # We can use the HTTPClient to make a POST request to the payments API here
            return {"net_fee":net_fee}
        except Exception as e:
            print("create_claim error: ", e)
            raise HTTPException(status_code=500, detail="Internal server error")
    
    # pseudo code to indicate how **claim_process** will communicate with **payments** and how to handle failure
    # def create_claim(self, claim: ClaimInput, session: Session = Depends(get_session)):
    #     try:
    #         # Start a transaction
    #         with session.begin():
    #             # Insert the claim into the database
    #             db_claim = Claim.from_orm(claim)
    #             self.claim_service.create_claim(db_claim, session)

    #             # Calculate the net fee
    #             net_fee = self.claim_service.calculate_net_fee(claim)

    #             # Call the payments API to process the payment
    #             # We can use the HTTPClient to make a POST request to the payments API here
    #             # We can also retry this post request a fixed number of times in case of failures
    #             payment_successful = True

    #             if payment_successful:
    #                 return net_fee
    #             else:
    #                 # If the payment was not successful, raise an error to rollback the transaction
    #                 raise ValueError("Payment failed")
    #     except SQLAlchemyError as e:
    #         # Handle the error and rollback the transaction
    #         session.rollback()
    #         raise e


    def get_claim_by_id(self, claim_id: int, session: Session = Depends(get_session)):
        try:
            task = session.get(Claim, claim_id)
            if not task:
                raise HTTPException(status_code=404, detail="Task not found")
            return task
        except Exception as e:
            print(" get_claim_by_id error: ", e)
            raise HTTPException(status_code=500, detail="Internal server error")


    def get_claims(self, skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
        try:
            tasks = session.query(Claim).offset(skip).limit(limit).all()
            return tasks
        except Exception as e:
            print(" get_claims error: ", e)
            raise HTTPException(status_code=500, detail="Internal server error")
