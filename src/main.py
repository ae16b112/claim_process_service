from http.client import HTTPException
from fastapi import FastAPI, Depends

from typing import List
from .database import create_db_and_tables, get_session
from .routers.index import claimRouter

app = FastAPI()

@app.on_event("startup")
def on_startup():
    create_db_and_tables()

@app.get("/ping")
def pong():
    return {"ping": "pong!"}

@app.get("/")
def say_hello():
    return {"message": "Welcome to the fastApi server"}

app.include_router(claimRouter)

