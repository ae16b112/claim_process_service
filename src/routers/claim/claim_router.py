from fastapi import APIRouter, Depends
from typing import List
from sqlmodel import Session

from ...models.claims import Claim
from ...schemas.claims import Payment, ClaimInput
from ...controllers.index import ClaimController
from ...database import create_db_and_tables, get_session

router = APIRouter()

claim_controller = ClaimController()

@router.post('/claims')
def create_claim(claim: ClaimInput, session: Session = Depends(get_session)):
    return claim_controller.create_claim(claim, session)

@router.get("/claims/{claim_id}", response_model=Claim)
def read_claim(claim_id: int, session: Session = Depends(get_session)):
    return claim_controller.get_claim_by_id(claim_id, session)

@router.get("/claims", response_model=List[Claim])
def read_claims(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    return claim_controller.get_claims(skip, limit, session)



# from fastapi import APIRouter, Depends
# from typing import List
# from sqlmodel import Session
# from ..models.claims import Claim
# from ..schemas.claims import Payment, ClaimInput
# from ..controllers.claim_controller import create_claim, read_claim, read_claims
# from ..database import create_db_and_tables, get_session


# router = APIRouter()

# @router.post('/claims')
# def create_claim(claim: ClaimInput, session: Session = Depends(get_session)):
#     return create_claim(claim, session)

# @router.get("/claim/{claim_id}", response_model=Claim)
# def read_claim(claim_id: int, session: Session = Depends(get_session)):
#     return read_claim(claim_id, session)

# @router.get("/claims", response_model=List[Claim])
# def read_claims(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
#     return read_claims(skip, limit, session)