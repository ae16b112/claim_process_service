from http.client import HTTPException
from pydantic import BaseModel, validator
from typing import Optional


class ClaimInput(BaseModel):
    service_date: str
    submitted_procedure: str
    quadrant: Optional[str]
    plan_group_number: str
    subscriber_number: str
    provider_npi: str
    provider_fees: float
    allowed_fees: float
    member_coinsurance: float
    member_copay: float
    
    @validator('submitted_procedure')
    def validate_submitted_procedure(cls, v):
        if not v.startswith('D'):
            raise ValueError('Submitted procedure must start with D')
        return v
    
    @validator('provider_npi')
    def validate_provider_npi(cls, v):
        if not v.isdigit() or len(v) != 10:
            raise ValueError('Provider NPI must be a 10 digit number')
        return v
    
    @validator('service_date', 'submitted_procedure', 'plan_group_number', 'subscriber_number', 'provider_npi', 'provider_fees', 'allowed_fees', 'member_coinsurance', 'member_copay')
    def validate_required_fields(cls, v, field):
        if not v:
            raise HTTPException(status_code=400, detail=f"{field['alias']} is required")
        return v
    
    
class Payment(BaseModel):
    id: int
    claim_id: int
    net_fee: float
