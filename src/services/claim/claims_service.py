from sqlmodel import Session
from ...models.claims import Claim

class ClaimService:
    def create_claim(self, claim: Claim, session: Session):
        session.add(claim)
        session.commit()
        session.refresh(claim)


    def calculate_net_fee(self, claim):
        # Compute the net fee
        net_fee = claim.provider_fees + claim.member_coinsurance + claim.member_copay - claim.allowed_fees
        return net_fee
